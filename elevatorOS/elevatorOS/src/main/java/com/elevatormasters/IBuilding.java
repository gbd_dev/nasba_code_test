/**
 * 
 */
package com.elevatormasters;

/**
 * The Interface IBuilding.
 *
 * @author deetuck
 */
public interface IBuilding {

    /** The building capacity. -----Data unknown.------ */
    public static final int MAX_BUILDING_CAPACITY = -1;

    /** The number of floors. */
    public static final int FLOORS = 10;

    /** The number of elevators. */
    public static final int ELEVATORS = 4;

    /**
     * The peak hours for the buildings high traffic.
     *
     * @return the building name
     */
    // List<PeakTime> peakHours;
    /**
     * @return the buildingName
     */
    String getBuildingName();

    /**
     * Gets the capacity.
     *
     * @return the capacity
     */
    int getCapacity();

    /**
     * Gets the number of floors.
     *
     * @return the numberOfFloors
     */
    int getNumberOfFloors();

    /**
     * Gets the number of elevators.
     *
     * @return the numberOfElevators
     */
    int getNumberOfElevators();

}