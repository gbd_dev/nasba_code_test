/**
 * 
 */
package com.elevatormasters;

/**
 * The Interface IElevator.
 *
 * @author deetuck
 */
public interface IElevator {

    /** The Constant MAX_ELEVATOR_CAPACITY. */
    public static final int MAX_ELEVATOR_CAPACITY = 12;

    /** The Constant TRAVEL_TIME. 2mins */
    public static final long TRAVEL_TIME = 120000;

}