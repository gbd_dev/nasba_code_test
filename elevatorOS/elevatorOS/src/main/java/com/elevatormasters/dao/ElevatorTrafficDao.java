package com.elevatormasters.dao;

import javax.sql.DataSource;

import generated.ElevatorRequest;
import generated.StatusType;

public interface ElevatorTrafficDao {

	void setDataSource(DataSource dataSource);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elevatormasters.dao.IElevatorTrafficDao#isCapacity(generated.
	 * ElevatorRequest)
	 */
	StatusType isCapacity(int requestedFloor);

	/**
	 * Get available slots on requested floor
	 * @param requestedFloor
	 * @return {@code Integer}floor availability
	 */
	Integer getAvailableCapacity(Integer requestedFloor);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.elevatormasters.dao.IElevatorTrafficDao#processElevatorRequest(generated.
	 * ElevatorRequest)
	 */
	StatusType processElevatorRequest(ElevatorRequest elevatorRequest);

	StatusType updateFloorTraffic(ElevatorRequest elevatorRequest);

	/**
	 * Decrement traffic on given floor by number of given load
	 * @param floor
	 * @param load
	 * @return {@code StatusType}}
	 */
	StatusType decrementFloorTraffic(int floor, int load);

	/**
	 * Increment traffic on given floor by number of given load
	 * @param floor
	 * @param load
	 * @return {@code StatusType}}
	 */
	StatusType incrementFloorTraffic(int floor, int load);

}