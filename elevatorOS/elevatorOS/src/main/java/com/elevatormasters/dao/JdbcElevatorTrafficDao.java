/**
 * 
 */
package com.elevatormasters.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.elevatormasters.IElevator;
import com.elevatormasters.bean.Floor;
import com.elevatormasters.service.ElevatorRequestQueue;

import generated.ElevatorRequest;
import generated.ElevatorSystem;
import generated.StatusType;

/**
 * Elevator Traffic DAO Layer
 * 
 * TODO: Extract Service and DAO layer
 * 
 * @author deetuck
 *
 */
@Component
public class JdbcElevatorTrafficDao implements ElevatorTrafficDao, IElevator {
	@Autowired
	Floor floor;

	private DataSource dataSource;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elevatormasters.dao.ElevatorTrafficDao#setDataSource(javax.sql.
	 * DataSource)
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * Check to see if the floor has reached its max capacity
	 * 
	 * @param requestedFloor
	 * @return {@code StatusType}
	 */
	public StatusType isCapacity(int requestedFloor) {

		Boolean isCapacity = getIsCapacity(requestedFloor);

		// invalid floor
		if (isCapacity == null) {

			return StatusType.FLOOR_INVALID;

		} else if (!isCapacity) {

			return StatusType.SPACE_AVAILABLE_ON_REQUEST_FLOOR;
		}

		return StatusType.REQUESTED_FLOOR_REACHED_CAPACITY;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.elevatormasters.dao.ElevatorTrafficDao#getAvailableCapacity(java.lang.
	 * Integer)
	 */
	public Integer getAvailableCapacity(Integer requestedFloor) {

		String sql = "SELECT * FROM FLOOR_TRAFFIC WHERE FLOOR (?) ";

		Connection conn = null;

		Integer availableCapacity = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, requestedFloor);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				//subject from max capacity
				availableCapacity = (floor.getCapacity() - rs.getInt("CURRENT_TRAFFIC"));
			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return availableCapacity;
	}
	
	/**
	 * Get current given floor traffic
	 * 
	 * @param a Floor
	 * @return current traffic
	 */
	public Integer getCurrentCapacity(Integer requestedFloor) {

		String sql = "SELECT * FROM FLOOR_TRAFFIC WHERE FLOOR (?) ";

		Connection conn = null;

		Integer availableCapacity = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, requestedFloor);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				availableCapacity = rs.getInt("CURRENT_TRAFFIC");
			}
			rs.close();
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return availableCapacity;
	}

	/**
	 * Check to see if the floor has reached its max capacity
	 * 
	 * @param requestedFloor
	 * @return Boolean - isCapacity
	 */
	private Boolean getIsCapacity(Integer requestedFloor) {

		// check capacity at requested floor

		String sql = "SELECT * FROM FLOOR_TRAFFIC WHERE FLOOR (?) ";

		Connection conn = null;

		Boolean isCapacity = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, requestedFloor);
			Integer capacity = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				capacity = rs.getInt("CURRENT_TRAFFIC");
			}
			rs.close();
			ps.close();

			if (capacity < floor.getCapacity()) {
				isCapacity = Boolean.FALSE;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return isCapacity;
	}

	/**
	 * Process Elevator Request Queue
	 * 
	 * @param floorWaitingArea
	 * @return {@code StatusType}
	 */
	public StatusType processElevatorRequest(int floor, ElevatorRequestQueue floorWaitingArea) {

		// check floor for capacity , how close are we to capacity?
		Integer available = getAvailableCapacity(floor);
		
		//if floor has atleast elevator max,
		
		StatusType requestStatus = null;
		
		if(available > MAX_ELEVATOR_CAPACITY) {
			
			int count = 0;
			
			do {
			  //grab first 12 off the queue and process, dequeue
			  ElevatorSystem elevatorRequest = floorWaitingArea.poll();
			  requestStatus  = processElevatorRequest(elevatorRequest.getElevatorSystemRequest());
			  count++;
			  
			  System.out.println("Elevator is on the " + floor + "clearing out the waiting/lobby area.");
			 
			}while( (!floorWaitingArea.isEmpty()) && count <= MAX_ELEVATOR_CAPACITY);
		}else {
			//just take as many as you can fit before reaching capacity on floor
			
			for (int counter = 1; counter <= available; counter++) {
				
				ElevatorSystem elevatorRequest = floorWaitingArea.poll();
				requestStatus  = processElevatorRequest(elevatorRequest.getElevatorSystemRequest());
				
				System.out.println("Elevator is on the " + floor + "clearing out the waiting/lobby area. Limited Availability");
				
			}//for
		}//else
		return requestStatus;
	}

	/**
	 * Process Elevator Request for Single Request
	 * 
	 * @param floorWaitingArea
	 * @return {@code StatusType}
	 */
	public StatusType processElevatorRequest(ElevatorRequest elevatorRequest) {
		// check floor for capacity (Elevator Floor Traffic Capacity)
		StatusType requestStatus = isCapacity(elevatorRequest.getRequestedFloor());

		if (requestStatus == StatusType.SPACE_AVAILABLE_ON_REQUEST_FLOOR) {

			requestStatus = updateFloorTraffic(elevatorRequest);
		}

		return requestStatus;
	}

	/**
	 * Process Elevator Request for Multiple Request
	 * 
	 * @param list
	 *            of {@code ElevatorRequest }
	 * @return {@code StatusType}
	 */
	public StatusType processElevatorRequest(List<ElevatorRequest> elevatorRequestList) {

		StatusType requestStatus = null;
		for (ElevatorRequest elevatorRequest : elevatorRequestList) {
			// verify requesting floor for space
			if (isCapacity(elevatorRequest.getRequestedFloor()) == StatusType.SPACE_AVAILABLE_ON_REQUEST_FLOOR) {
				requestStatus = processElevatorRequest(elevatorRequest);
			}
		}
		return requestStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elevatormasters.dao.ElevatorTrafficDao#updateFloorTraffic(generated.
	 * ElevatorRequest)
	 */
	public StatusType updateFloorTraffic(ElevatorRequest elevatorRequest) {

		// //get current floor
		int currentFloor = elevatorRequest.getCurrentFloor();

		// get request floor
		int requestedFloor = elevatorRequest.getRequestedFloor();

		int singleRider = 1;

		StatusType status = null;

		// last one wins, rushing lol :)

		// increment requested
		status = incrementFloorTraffic(requestedFloor, singleRider);

		//not pretty, jdbc doesnt like my sql, TODO
		if(getCurrentCapacity(currentFloor) != 0) {
			// decrement current
		status = decrementFloorTraffic(currentFloor, singleRider);
		}
		

		return status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elevatormasters.dao.ElevatorTrafficDao#decrementFloorTraffic(int,
	 * int)
	 */
	public StatusType decrementFloorTraffic(int floor, int load) {

		String sql = "UPDATE FLOOR_TRAFFIC SET CURRENT_TRAFFIC = CURRENT_TRAFFIC - (?) where floor = (?);";

		Connection conn = null;

		StatusType status = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, load);
			ps.setInt(2, floor);
			int rs = ps.executeUpdate();
			
			if (rs == 1) {
				status = StatusType.REQUEST_COMPLETE;
					
				}else {
					 status = StatusType.ERROR_PROCESSING_REQUEST;
				}
			
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.elevatormasters.dao.ElevatorTrafficDao#incrementFloorTraffic(int,
	 * int)
	 */
	public StatusType incrementFloorTraffic(int floor, int load) {

		String sql = "UPDATE FLOOR_TRAFFIC SET CURRENT_TRAFFIC = CURRENT_TRAFFIC + (?) where floor = (?);";

		Connection conn = null;

		StatusType status = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, load);
			ps.setInt(2, floor);
			int rs = ps.executeUpdate();
			if (rs == 1) {
				status = StatusType.REQUEST_COMPLETE;
			} else {
				status = StatusType.ERROR_PROCESSING_REQUEST;
			}
			ps.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}

		return status;
	}

}
