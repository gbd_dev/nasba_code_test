package com.elevatormasters.bean;

import org.springframework.stereotype.Component;

import com.elevatormasters.IFloor;
@Component
public class Floor extends Building implements IFloor {

    /*
     * (non-Javadoc)
     * 
     * @see com.elevatormasters.bean.Building#getCapacity()
     */
    @Override
    public int getCapacity() {
	// TODO Auto-generated method stub
	return MAX_TRAFFIC_CAPACITY;
    }

}