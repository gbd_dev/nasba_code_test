/*
 * This class 
 */
package com.elevatormasters.bean;

import org.springframework.stereotype.Component;

import com.elevatormasters.IBuilding;

/**
 * The Class Building.
 */

public class Building implements IBuilding {

    /** The building name. */
    String buildingName;

    /**
     * The peak hours for the buildings high traffic.
     **/
    // List<PeakTime> peakHours;
    /*
     * (non-Javadoc)
     * 
     * @see com.elevatormasters.bean.IBuilding#getBuildingName()
     */
    public String getBuildingName() {
	return buildingName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.elevatormasters.bean.IBuilding#getCapacity()
     */
    public int getCapacity() {
	return MAX_BUILDING_CAPACITY;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.elevatormasters.bean.IBuilding#getNumberOfFloors()
     */
    public int getNumberOfFloors() {
	return FLOORS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.elevatormasters.bean.IBuilding#getNumberOfElevators()
     */
    public int getNumberOfElevators() {
	return ELEVATORS;
    }

}
