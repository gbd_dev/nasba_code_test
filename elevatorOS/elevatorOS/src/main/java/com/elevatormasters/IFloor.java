/**
 * 
 */
package com.elevatormasters;

/**
 * The Interface IFloor.
 *
 * @author deetuck
 */
public interface IFloor {

    /** The Constant MAX_TRAFFIC_CAPACITY. */
    public static final int MAX_TRAFFIC_CAPACITY = 100;

}