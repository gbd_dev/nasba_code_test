
package com.elevatormasters.service;

import java.util.LinkedList;
import java.util.Queue;

import generated.ElevatorSystem;
import generated.StatusType;

/**
 * Thread-Safe Queue implementation for handling ElevatorSystem.
 *
 * @author deetuck
 */
public class ElevatorRequestQueue {

	/** The queue. */
	static Queue<ElevatorSystem> ElevatorSystemQueue = new LinkedList<ElevatorSystem>();

	/** The queue instance. */
	private static ElevatorRequestQueue queueInstance = null;

	/**
	 * Gets the single instance of ElevatorSystemQueue.
	 *
	 * @return single instance of ElevatorSystemQueue
	 */
	public static ElevatorRequestQueue getInstance() {

		if (queueInstance == null) {
			queueInstance = new ElevatorRequestQueue();
		}
		return queueInstance;
	}

	/**
	 * Gets the current elevator request queue.
	 *
	 * @return the current elevator request queue
	 */
	public Queue<ElevatorSystem> getCurrentElevatorSystemQueue() {
		return ElevatorSystemQueue;
	}

	/**
	 * Adds the ElevatorSystem object to the queue.
	 *
	 * @param request
	 *            the ElevatorSystem
	 */
	public StatusType add(ElevatorSystem request) {
		synchronized (ElevatorSystemQueue) {
			System.out.println("Request Added to Pending Queue[" + "Current Floor:"
					+ request.getElevatorSystemRequest().getCurrentFloor() + "," + "Requested Floor: "
					+ request.getElevatorSystemRequest().getRequestedFloor() + "]");
			ElevatorSystemQueue.add(request);
			return StatusType.PENDING;
		}
	}

	/**
	 * Removes the @param ElevatorSystem from the queue.
	 *
	 * @param ElevatorSystem
	 *            the ElevatorSystem
	 */
	public void remove(ElevatorSystem ElevatorSystem) {
		synchronized (ElevatorSystemQueue) {
			ElevatorSystemQueue.remove(ElevatorSystem);
		}
	}

	/**
	 * Retrieves the head of the queue then removes the object from the queue.
	 *
	 * @return the elevator request null if queue is empty
	 */
	public ElevatorSystem poll() {
		ElevatorSystem data = ElevatorSystemQueue.poll();
		return data;
	}

	/**
	 * Checks if queue is empty.
	 *
	 * @return true, if the queue is empty
	 */
	public boolean isEmpty() {
		return ElevatorSystemQueue.isEmpty();
	}

	/**
	 * Gets the total size of the queue.
	 *
	 * @return the total size of the queue
	 */
	public int getTotalSize() {
		return ElevatorSystemQueue.size();
	}
}