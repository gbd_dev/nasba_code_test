/**
 * 
 */
package com.elevatormasters.service;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.stereotype.Component;

import generated.ElevatorResponse;
import generated.ElevatorSystem;

/**
 * Responsible for converting object to XML payloads
 * @author deetuck
 *
 */
@Component
public class XmlMarshallerService {
	
	/**
	 * Converts {@code ElevatorResponse} to String / XML
	 * @param elevatorResponse
	 * @return null or XML payload
	 */
	public String xmlMarshaller(ElevatorSystem elevatorSystemResponse) {
		 try {
	            JAXBContext context = JAXBContext.newInstance(ElevatorSystem.class);
	            Marshaller m = context.createMarshaller();
	            //format the XML payload for pretty indentation 
	            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

	            final StringWriter xmlStringWriter = new StringWriter();
	            m.marshal(elevatorSystemResponse, xmlStringWriter);
	            return xmlStringWriter.toString();
	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
		return "Fatal Error Occured Creating XML Response";
	}

}
