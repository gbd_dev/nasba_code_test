package com.elevatormasters.controller;

import java.util.LinkedList;
import java.util.Queue;


/**
 * Thread-Safe Queuing system for Idle Elevator, 
 * TODO: Generics ?
 * @author deetuck
 *
 */
public class IdleElevatorQueue {

	/** The queue. */
    static Queue<Elevator> availableElevatorQueue = new LinkedList<Elevator>();

    /** The queue instance. */
    private static IdleElevatorQueue queueInstance = null;

    /**
     * Gets the single instance of IdleElevatorQueue.
     *
     * @return single instance of IdleElevatorQueue
     */
    public static IdleElevatorQueue getInstance() {

	if (queueInstance == null) {
	    queueInstance = new IdleElevatorQueue();
	}
	return queueInstance;
    }

    /**
     * Gets the current IdleElevatorQueue
     *
     * @return the current IdleElevatorQueue
     */
    public Queue<Elevator> getCurrentIdleElevatorQueue() {
	return availableElevatorQueue;
    }

    /**
     * Adds the Elevator object to the queue.
     *
     * @param request
     *            the Elevator
     */
    public void add(Elevator request) {
	synchronized (availableElevatorQueue) {
		System.out.println("Elevator Added Back to the queue");
	    availableElevatorQueue.add(request);
	}
    }

    /**
     * Removes the @param Elevator from the queue.
     *
     * @param Elevator
     *            the Elevator
     */
    public void remove(Elevator Elevator) {
	synchronized (availableElevatorQueue) {
	    availableElevatorQueue.remove(Elevator);
	}
    }

    /**
     * Retrieves the head of the queue then removes the object from the queue.
     *
     * @return an idle elevator, null if queue is empty
     */
    public Elevator poll() {
	Elevator data = availableElevatorQueue.poll();
	return data;
    }

    /**
     * Checks if queue is empty.
     *
     * @return true, if the queue is empty
     */
    public boolean isEmpty() {
	return availableElevatorQueue.isEmpty();
    }

    /**
     * Gets the total size of the queue.
     *
     * @return the total size of the queue
     */
    public int getTotalSize() {
	return availableElevatorQueue.size();
    }
}
