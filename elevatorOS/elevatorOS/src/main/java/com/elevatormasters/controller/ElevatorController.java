package com.elevatormasters.controller;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.elevatormasters.bean.Building;
import com.elevatormasters.dao.ElevatorTrafficDao;
import com.elevatormasters.service.ElevatorRequestQueue;
import com.elevatormasters.service.XmlMarshallerService;

import generated.ElevatorRequest;
import generated.ElevatorResponse;
import generated.ElevatorSystem;
import generated.StatusType;

/**
 * 
 * Controller to process elevator request
 * 
 * @author deetuck
 *
 */
@RestController
@Component
public class ElevatorController {
	/**
	 * Number of Elevators in the Building
	 */
	private static final int MAX_ELEVATORS = 4;

	@Autowired
	private ElevatorTrafficDao elevatorTrafficDao;
	@Autowired
	private XmlMarshallerService xmlMarshallerService;

	@Autowired
	private Building buiding;

	/**
	 * Request and Elevator Queue(s)
	 */

	private IdleElevatorQueue idleElevatorQueue = IdleElevatorQueue.getInstance();
	private HashMap<Integer, ElevatorRequestQueue> pendingElevatorRequest;

	/**
	 * On Application startup populate elevator queue, and intialize each floors waiting area
	 * 
	 * @param event
	 */
	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {

		//create waiting area on each floor
		buildWaitingAreaOnEachFloor();
		// create elevators at application start up
		createElevators();

	}

	/**
	 * Setup repository for pending request
	 */
	private void buildWaitingAreaOnEachFloor() {

		// TODO: Revisit this
		// setup repository for pending request
		pendingElevatorRequest = new HashMap<Integer, ElevatorRequestQueue>();

		// Initialize Map
		for (int floor = 1; floor <= Building.FLOORS; floor++) {

			// Each Floor in the building should have its own unique waiting area
			ElevatorRequestQueue elevatorRequestQueue = null;
			elevatorRequestQueue = ElevatorRequestQueue.getInstance();

			pendingElevatorRequest.put(floor, elevatorRequestQueue);
		}
	}
	
	/**
	 * Create elevators and load Elevators in the idle elevator queue
	 */
	private void createElevators() {
		
		for (int i = 1; i <= Building.ELEVATORS; i++) {
			Elevator elevator = new Elevator("Elevator " + i);
			idleElevatorQueue.add(elevator);
		}
	}

	/**
	 * Processes all incoming Elevator Request
	 * 
	 * @param elevatorSystemRequest
	 *            {@code ElevatorSystem}
	 * @return {@code ElevatorResponse}
	 */
	@RequestMapping(value = "request", method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public String handleElevatorRequest(@RequestBody ElevatorSystem elevatorSystemRequest) {

		StatusType requestStatus;

		// no elevators available
		if (idleElevatorQueue.isEmpty()) {

			int currentFloor = elevatorSystemRequest.getElevatorSystemRequest().getCurrentFloor();
			// no elevators so, add request to request queue
			requestStatus = pendingElevatorRequest.get(currentFloor).add(elevatorSystemRequest);

			// return status that say pending with id, id and request store in the table,
			// table updated when fufilled

		} else {

			requestStatus = processElevatorRequest(elevatorSystemRequest);
		}

		// build response, and return to sender
		return buildResponseBody(elevatorSystemRequest, requestStatus);

	}

	/**
	 * Elevator is available, so lets process the request
	 * 
	 * @param elevatorSystemRequest
	 * @return {@code StatusType}}
	 */
	private StatusType processElevatorRequest(ElevatorSystem elevatorSystemRequest) {
		StatusType requestStatus;
		{

			// elevator available
			Elevator availableElevator = idleElevatorQueue.poll();

			// start elevator thread
			requestStatus = availableElevator.openElevator();

			if (requestStatus == StatusType.IN_PROGRESS) {
				// process elevator request
				requestStatus = elevatorTrafficDao
						.processElevatorRequest(elevatorSystemRequest.getElevatorSystemRequest());

				// wait for elevator to finish, and send elevator back to idle elevator queue
				Boolean isElevatorReady = availableElevator.readyForNextRequest();

				if (isElevatorReady) {
					// add elevator back the queue
					idleElevatorQueue.add(availableElevator);
				} // isElevatorFinished
			} // space available on floor
		}
		return requestStatus;
	}

	/**
	 * Build the response
	 * 
	 * @param elevatorSystemRequest
	 * @param requestStatus
	 * @return String, Response XML Payload
	 */
	private String buildResponseBody(ElevatorSystem elevatorSystem, StatusType requestStatus) {

		// grab request info, to display in the response to the user
		ElevatorRequest request = elevatorSystem.getElevatorSystemRequest();
		int currentFloor = request.getCurrentFloor();
		int requestedFloor = request.getRequestedFloor();

		// create response object
		ElevatorResponse elevatorResponse = new ElevatorResponse();
		elevatorResponse.setStatus(requestStatus);
		elevatorResponse.setCurrentFloor(currentFloor);
		elevatorResponse.setRequestedFloor(requestedFloor);

		// add response to ElevatorSystem object
		elevatorSystem.setElevatorSystemResponse(elevatorResponse);

		// dont display the intial request in the response
		elevatorSystem.setElevatorSystemRequest(null);

		// call marshaller service to create XML Response payload
		String xmlResponse = xmlMarshallerService.xmlMarshaller(elevatorSystem);

		// return xml response
		return xmlResponse;
	}

}
