package com.elevatormasters.controller;

import org.springframework.stereotype.Component;

import com.elevatormasters.IElevator;

import generated.StatusType;

class Elevator implements Runnable, IElevator {

	public Thread elevatorThread;
	private String elevatorName;
	boolean suspended = false;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            Elevator Name, determined at app startup see controller
	 */
	public Elevator(String name) {
		elevatorName = name;
		System.out.println("Creating " + elevatorName);
	}

	/**
	 * Make elevator available again
	 */
	synchronized void resume() {
		suspended = false;
		System.out.println(elevatorName + " is available for next request.");
		notify();
	}

	/**
	 * Start Elevator
	 * 
	 * @return {@code StatusType}
	 */
	public StatusType openElevator() {

		try {
			start();

		} catch (Exception e) {
			return StatusType.ERROR_REQUESTING_ELEVATOR;
		}

		// send status back to user, no waiting on thread to finish
		return StatusType.IN_PROGRESS;
	}

	/*
	 * Return status of Thread, if true then idle, if false elevator in motion
	 */
	public Boolean readyForNextRequest() {

		return suspended;
	}

	/**
	 * Simulate Elevator Traveling
	 */
	public void run() {

		try {
			System.out.println(elevatorName + " is traveling.");
			Thread.sleep(TRAVEL_TIME);
			resume();

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Start thread
	 */
	public void start() {
		System.out.println("Starting " + elevatorName);
		if (elevatorThread == null) {
			elevatorThread = new Thread(this, elevatorName);
			elevatorThread.start();
		}
	}

	/**
	 * Suspend Thread
	 */
	void suspend() {
		suspended = true;
	}

}